# Native Raspberry Drive

By Tadeusz Puźniakowski

Allow running grbl compatible g-code on raspberry pi withoud grbl-shield. Just native GPIO state manipulation.

## Getting started

Compile it:

```bash
make
```

Run it:

```bash
./build/pigcd
```

You can also set some more advanced options, for example, allow for generation of g-code.

```bash
./build/pigcd webapi::static_files_directory ../pifront/build pigcd::togcd_path ../togcd/build
```

## Interaction via HTTP

```bash
curl ....
```

There is endpoint that shows API documentation ```/apidoc```

### Default settings

#### Stepstick configuration:

| role                  | X   | Y   | Z     |
|----------------------:|-----|-----|-------|
| dir                   | 27  | 4   | 9     |
| en                    | 10  | 10  | 10    |
| step                  | 22  | 17  | 11    |
| steps_per_millimeter  | 100 | 100 | -1600 |

Spindle pin: 18


#### The example connection schematic for stepstics.

![connections](connection-v3-json.png)


## Roadmap

TODO

## Contributing

TOD

## Authors and acknowledgment

Tadeusz Puźniakowski

## License

See [LICENSE](LICENSE).
