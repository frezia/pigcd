#!/bin/bash -e

H="build_$(hostname)"
P=$PWD

mkdir -p "$H"
cd "$H"
echo "cmake.."
cmake ../ -DCMAKE_BUILD_TYPE=Release
cd "$P"
echo "building..."
cmake --build "$H"

echo "Running test environment"
$H/pigcd webapi::static_files_directory ../pifront/build pigcd::togcd_path ../togcd/build config_file_name ./localconfig.conf
