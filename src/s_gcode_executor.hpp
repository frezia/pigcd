#ifndef __SERVICE_GCODE_EXECUTOR__HPP____
#define __SERVICE_GCODE_EXECUTOR__HPP____
/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <atomic>
#include <future>
#include <json.hpp>
#include <list>
#include <string>
#include <vector>
namespace pigcd {
namespace service {

class gcode_executor_t {
    std::vector<std::string> _status_exec;
    std::vector<std::string> _lines;
    std::future<int> _execution;
    std::mutex _mutex;
    std::atomic<bool> _on_hold;
    std::atomic<int> _program_id;

    void push_back(const std::string s);
    void clear();
    std::size_t exec_index();
    static int gcode_executor(gcode_executor_t *pthis, const std::list<std::string> lines);

    nlohmann::json cycle_hold(std::list<std::string>);
    nlohmann::json cycle_start(std::list<std::string>);
    nlohmann::json current_status_compact(std::list<std::string>);
    nlohmann::json cycle_cancel(std::list<std::string>);

   public:
    bool is_gcode_executing();

    std::atomic<bool> ignore_next_gcode = false;

    /**
     * @brief Get the status object that contains executed commands starting with
     * first_idx
     *
     * @param first_idx first index to get
     * @return nlohmann::json
     */
    nlohmann::json get_status(unsigned first_idx = 0);

    nlohmann::json exec(const std::list<std::string> &lines);
};
}  // namespace service
}  // namespace pigcd
#endif
