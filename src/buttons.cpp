/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "buttons.hpp"

#include <algorithm>
#include <array>
#include <chrono>
#include <future>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <thread>
#include <vector>

#include "configuration.hpp"
#include "gpio.hpp"

namespace pigcd {

namespace buttons {

std::vector<int> io_buttons = {21, 20, 16, 12};

std::vector<int> get_buttons_state() {
    std::vector<int> ret(io_buttons.size());
    std::transform(io_buttons.begin(), io_buttons.end(), ret.begin(), gpio::get_state);
    return ret;
}

}  // namespace buttons

}  // namespace pigcd