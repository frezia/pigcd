/**
 * @file gpio-x86_64.cpp
 * @author Tadeusz Puźniakowski
 * @brief Fake implementation of steps generation on the PC - allows for
 * checking if steps are generated correctly
 * @version 0.1
 * @date 2022-01-31
 *
 * @copyright Copyright (c) 2022
 *
 */
/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#ifdef ARCH_x86_64

#include <iostream>

#include "gpio.hpp"
//#include <chrono>
#include <list>
#include <thread>

#include "motors.hpp"

namespace pigcd {

uint64_t get_precision_time();

namespace gpio {

int pin_states_fake[32];
std::list<std::vector<int64_t>> reported_fake_states;
void print_reported_fake_states() {
    const auto coordinates = motors::io_steppers_dir.size();
    std::cout << "# ";
    for (int i = 1; i < 32; i++) std::cout << i << "\t";
    std::cout << "T\tt\tx\ty\tz";
    std::cout << std::endl;

    auto update_x_y_z = [coordinates](auto states, auto prev_states, const std::vector<int> xyz_pos_, auto on_coordinate_step) -> std::vector<int> {
        auto xyz_pos = xyz_pos_;
        for (unsigned i = 0; i < states.size(); i++) {
            int pin = i + 1;
            if (states[i] > prev_states[i]) {
                int found_step_idx = -1;
                for (unsigned idx = 0; idx < coordinates; idx++) {
                    if (motors::io_steppers_step[idx] == pin) found_step_idx = idx;
                }
                if (found_step_idx >= 0) {
                    int direction_step = ((states[motors::io_steppers_dir[found_step_idx] - 1]) ? 1 : -1);
                    xyz_pos[found_step_idx] = xyz_pos[found_step_idx] + direction_step;
                    on_coordinate_step(found_step_idx);
                }
            }
        }
        return xyz_pos;
    };
    auto prev_states = pigcd::gpio::reported_fake_states.front();
    std::vector<int> xyz(coordinates);
    std::vector<int> last_xyz(coordinates);
    std::vector<int> velocity_xyz(coordinates);
    for (auto& states : pigcd::gpio::reported_fake_states) {
        for (auto& v : velocity_xyz) v = (v > 100) ? (v - 100) : 0;
        xyz = update_x_y_z(states, prev_states, xyz, [&last_xyz, &velocity_xyz, states](int t) {
            auto prev_tick = last_xyz[t];
            last_xyz[t] = states.back();
            if ((prev_tick - last_xyz[t]) != 0) {
                velocity_xyz[t] = 100000000 / (last_xyz[t] - prev_tick);
            } else {
                velocity_xyz[t] = -1;
            }
        });

        for (unsigned i = 0; i < states.size(); i++) {
            std::cout << states[i] << "\t";
        }
        for (auto v : xyz) std::cout << v << "\t";
        for (auto v : velocity_xyz) std::cout << ((v > 0.0) ? std::to_string(v) : "") << "\t";
        std::cout << std::endl;
        prev_states = states;
    }
}

auto& output = std::cout;

void init_in(const std::vector<int> pins, const std::vector<int> pullups) {
    output << "# init_in [";
    for (auto p : pins) {
        output << " " << p;
    }
    output << " ] [";
    for (auto p : pullups) {
        output << " " << p;
    }
    output << "]" << std::endl;
}
void init_out(const std::vector<int> pins) {
    output << "# init_out [";
    for (auto p : pins) {
        output << " " << p;
    }
    output << "]" << std::endl;
}

inline void print_pin_state(const std::vector<int> state, int64_t t) {
    if (reported_fake_states.size() < 100000) {
        std::vector<int64_t> tolist;
        tolist.reserve(48);
        for (unsigned i = 1, statex = pin_states_fake[1]; i < state.size(); ++i, statex = state[i]) {
            tolist.push_back(statex);
        }
        tolist.push_back(t);
        tolist.push_back((reported_fake_states.size()) ? ((long int)t - (long int)reported_fake_states.front().at(31)) : (long int)0);
        reported_fake_states.push_back(std::move(tolist));
    }
}
void set_states(const std::vector<int>& pins, const std::vector<char>& states) {
    print_pin_state(std::vector<int>(pin_states_fake, pin_states_fake + 32), get_precision_time());
    for (unsigned i = 0; i < pins.size(); i++) {
        if ((states.at(i) == 0) || (states.at(i) == 1)) pin_states_fake[pins.at(i)] = states.at(i);
    }
    print_pin_state(std::vector<int>(pin_states_fake, pin_states_fake + 32), get_precision_time());
}
char get_state(const int pin) {
    // output << "# get_state " << pin << "  <-- " << pin_states_fake[pin]
    //        << std::endl;
    return pin_states_fake[pin];
}

}  // namespace gpio
}  // namespace pigcd

#endif
