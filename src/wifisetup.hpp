#ifndef WIFISETUP_HPP____
#define WIFISETUP_HPP____

#include <vector>
#include <string>
#include <map>

namespace pigcd {

std::vector<std::string> get_wifi_list();

std::map<std::string, std::string> get_wpa_passwords();


void set_wpa_password(std::string ssid, std::string password);

}  // namespace pigcd

#endif