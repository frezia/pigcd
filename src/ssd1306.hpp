#ifndef __SSD_1306_PRINTING_INFO_HPP___
#define __SSD_1306_PRINTING_INFO_HPP___
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include <ifaddrs.h>
#include <arpa/inet.h>

#include <string>
#include <vector>
#include <iostream>

namespace pigcd {

class display_ssd1306_t {
public:
    std::vector<std::vector<unsigned char>> _display;
    int _addr; ///< device address
    int _handle; ///< file handle to i2c device

    void write_buff(const std::vector < unsigned char > data);
    void blit();
    void put_bit(int x, int y, int c);
    void print_bitmap_8x8(int x0, int y0, const unsigned char *p) ;
    /**
     * @brief Print single digit on a buffer
     * 
     * @param x position x
     * @param y position y
     * @param digit digit. It only supports '0'-'9', '.', ' '
     */
    void print_character(int x, int y, char digit) ;
    void print_string(int x, int y, const std::string s) ;
    display_ssd1306_t(const int address_ = 0x3C, const std::string &devfile_ = "/dev/i2c-1");
    virtual ~display_ssd1306_t();
};

}

#endif
