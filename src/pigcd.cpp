/**
 * @file main.cpp
 * @author Tadeusz Puźniakowski
 * @brief Allows for execution of simple gcode directly on raspberry pi
 * @version 0.1
 * @date 2022-01-31
 *
 * @copyright Copyright (c) 2022
 *
 */
/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <openssl/sha.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

#include <chrono>
#include <distance_t.hpp>
#include <fstream>
#include <iostream>
#include <json.hpp>
#include <list>
#include <map>
#include <random>
#include <regex>
#include <sstream>
#include <thread>
#include <vector>
#include <webapi.hpp>

#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include <ifaddrs.h>
#include <arpa/inet.h>


#include "ssd1306.hpp"
#include "buttons.hpp"
#include "configuration.hpp"
#include "gcd.hpp"
#include "gpio.hpp"
#include "motors.hpp"
#include "pigcd.hpp"
#include "s_gcode_executor.hpp"
#include "unit_conversion.hpp"
#include "vec3di.hpp"
#include "wifisetup.hpp"

using namespace tp::discrete;

namespace pigcd {
std::string togcd_path = "../togcd/build";

namespace gpio {
#ifdef ARCH_x86_64
#warning "Using fake driver on x86 architecture. If you want it to work, compile it on the Raspberry Pi 2 or 3"
extern std::list<std::vector<unsigned int>> reported_fake_states;
void print_reported_fake_states();
#endif
}  // namespace gpio
}  // namespace pigcd

pigcd::service::gcode_executor_t gcode_executor_server_obj;

struct execution_task_c {
    std::future<nlohmann::json> ret;
    std::chrono::system_clock::time_point t0;
};
std::map<std::string, execution_task_c> execution_tasks;

nlohmann::json new_execution_task(std::function<nlohmann::json()> f) {
    static std::random_device rd;
    static std::mt19937_64 gen(rd());
    static std::uniform_int_distribution<int> dist(100000000, 999999999);
    std::string id = std::to_string(dist(gen));
    execution_tasks[id] = {std::async(std::launch::async,f),std::chrono::system_clock::now()};
    return {{"id", id}};
}

nlohmann::json get_execution_task(const std::string id) {
    using namespace std::chrono_literals;
    if (execution_tasks.count(id) == 0) {
        return {{"id", id}, {"status", "not_found"}};
    }
    auto &future = execution_tasks[id].ret;
    std::future_status status = future.wait_for(30s);
    switch (status) {
        case std::future_status::deferred:
            execution_tasks.erase(id);
            return {{"id", id}, {"status", "deferred"}};
            break;
        case std::future_status::timeout:
            return {{"id", id}, {"status", "timeout"}};
            break;
        case std::future_status::ready:
            nlohmann::json value = future.get();
            execution_tasks.erase(id);
            nlohmann::json ret_j = {{"id", id}, {"status", "ready"}};
            ret_j["value"] = value;
            return ret_j;
            break;
    }
    return {{"id", id}, {"status", "unknown_error"}};
}

void request_handler_GET_api_gcode_status(pigcd::webapi::request_data_t &, pigcd::webapi::response_data_t &response, const std::vector<std::string> &params) {
    try {
        response.send(gcode_executor_server_obj.get_status(std::stoi(params.at(1))).dump());
    } catch (std::exception &e) {
        std::string error;
        error = "error parsing params: ";
        for (auto p : params) {
            error = error + "[" + p + "] ";
        }
        std::cerr << error << std::endl;
        throw std::invalid_argument(error);
    }
}
void request_handler_GET_api_gcode_status_all(pigcd::webapi::request_data_t &, pigcd::webapi::response_data_t &response, const std::vector<std::string> &) {
    response.send(gcode_executor_server_obj.get_status(0).dump());
}

void request_handler_POST_exec(pigcd::webapi::request_data_t &request, pigcd::webapi::response_data_t &response, const std::vector<std::string> &) {
    using namespace pigcd;

    auto [method, url, headers, body, user_data] = request;

    auto body_string = std::string(body.begin(), body.end());

    std::stringstream input_lines(body_string);
    std::list<std::string> lines;
    std::string l;
    while (std::getline(input_lines, l)) lines.push_back(l);
    response.send(gcode_executor_server_obj.exec(lines).dump());
}

std::string exec_command(const std::string cmd) {
    std::array<char, 1024> buffer;
    std::string result;

    auto pipe = popen(cmd.c_str(), "r");  // get rid of shared_ptr
    if (!pipe) throw std::runtime_error("popen() failed!");
    while (!feof(pipe)) {
        if (fgets(buffer.data(), buffer.size(), pipe) != nullptr) result += buffer.data();
    }
    auto rc = pclose(pipe);
    if (rc == EXIT_SUCCESS) {
        return result;
    } else {
        throw std::runtime_error("ERROR running " + cmd + ":" + std::to_string(rc));
    }
}

std::string togcd_status_filename = std::string("/tmp/togcd_status_") + std::to_string(getpid()) + ".log";

void request_handler_POST_image_to_gcode(pigcd::webapi::request_data_t &request, pigcd::webapi::response_data_t &response, const std::vector<std::string> &) {
    using namespace pigcd;

    auto [method, url, headers, body, user_data] = request;

    auto body_string = std::string(body.begin(), body.end());
    // std::cout << "body_string: " << body_string << std::endl;
    nlohmann::json body_json = nlohmann::json::parse(body_string);


    auto data = pigcd::webapi::base64tobin(body_json["image"]);
    std::ofstream f("/tmp/result_data.png");
    f.write(data.data(), data.size());
    f.close();
    /*

 -dpi                      resolution of the source image [dpi]. (30.000000-1200.000000)
 -drill_depth              how deep do we need to cut the material [mm] (0.000000-30.000000)
 -file                     The input file name.
 -g1drillfeedrate          speed of drill movements [mm/s] (10.000000-120.000000)
 -g1feedrate               speed of cutting [mm/s] (0.500000-120.000000)
 -help                     The help screen.
 -multipass                how many cutting iterations (1.000000-100.000000)
 -safe_cut                 how high should be the left over connection that keeps elements in material (0.000000-255.000000)
 -tool_d                   diameter of the tool [mm] (0.000000-5.000000)
 -tool_d_areas             the thickness of areas cut or 0 if no areas [mm] (0.000000-5.000000)
 -status_fname             the status progress
 -dp_epsilon               Ramer–Douglas–Peucker epsilon
    */
    static const auto settingNames =
        std::vector<std::string>{"dpi", "drill_depth", "file", "g1drillfeedrate", "g1feedrate", "help", "multipass", "safe_cut", "tool_d", "tool_d_areas", "dp_epsilon"};
    std::string params = "-status_fname " + togcd_status_filename + " ";
    for (auto k : settingNames) {
        try {
            params = params + "-" + k + " " + std::to_string((double)body_json["settings"][k]) + " ";
        } catch (...) {
        }
    }
    std::string cmnd = pigcd::togcd_path + "/togcd -file /tmp/result_data.png " + params;
    std::cout << "settings: " << (body_json["settings"].dump()) << std::endl;
    std::cout << "running: " << cmnd << std::endl;
    //auto fut = std::async(std::launch::async, );
    response.send(new_execution_task([cmnd]() -> nlohmann::json {
        std::string ret_str = exec_command(cmnd);
        return {{"gcode", ret_str}};
    }).dump());
    // response.send(exec_command(cmnd));
}

/**
 * @brief Set the wifi password object
 * 
 * You can check it with:
 *  curl -d '{"ssid":"demossid","psk":"demopsk"}' http://localhost:12112/api/setwifipass -H "Content-Type: application/json"
 * 
 * @param request 
 * @param response 
 */
void request_handler_POST_set_wifi_password(pigcd::webapi::request_data_t &request, pigcd::webapi::response_data_t &response, const std::vector<std::string> &) {
    using namespace pigcd;

    auto [method, url, headers, body, user_data] = request;

    auto body_string = std::string(body.begin(), body.end());
    nlohmann::json body_json = nlohmann::json::parse(body_string);

    response.send([body_json]() -> nlohmann::json {
        std::string ssid = body_json["ssid"];
        std::string psk = body_json["psk"];
        set_wpa_password(ssid, psk);
        return {{"result", "OK"}};
    }().dump());
}

/**
 * @brief Get the wifi list object
 * 
 * You can check it with:
 *  curl http://localhost:12112/api/getwifilist
 * 
 * @param response 
 */
void request_handler_GET_wifi_list(pigcd::webapi::request_data_t &, pigcd::webapi::response_data_t &response, const std::vector<std::string> &) {
    using namespace pigcd;

    response.send([]() -> nlohmann::json {
        auto cards_and_wifis = get_wifi_list();
        auto wpa_passwords = get_wpa_passwords();
        std::map<std::string, std::string> wpa_passwords_and_ssid;
        for (auto &c : cards_and_wifis) {
            if (wpa_passwords.count(c) > 0) {
                wpa_passwords_and_ssid[c] = wpa_passwords[c];
            } else {
                wpa_passwords_and_ssid[c] = "";
            }
        }
        for (auto [k,p] : wpa_passwords) {
            if (wpa_passwords_and_ssid.count(k) == 0) {
                wpa_passwords_and_ssid[k] = p;
            }
        }
        std::vector<std::vector<std::string>> passwords;
        for (auto [k, p] : wpa_passwords_and_ssid) {
            passwords.push_back({k, p});
        }
        nlohmann::json ret = {
            {"status", "OK"},
            {"wifi_list", passwords}
        };
        return ret;
    }().dump());
}





std::atomic<int> finish_main_threads = 0;
void on_ctrl_c_signal(int) {
    pigcd::motors::is_thread_working = 0;  // terminate motors thread
    gcode_executor_server_obj.ignore_next_gcode = true;
    finish_main_threads = 1;
}

void on_usr1_signal(int) { gcode_executor_server_obj.ignore_next_gcode = true; }

// todo: session handling
void session_handling_filter(pigcd::webapi::request_data_t &request, pigcd::webapi::response_data_t &/*response*/, const std::vector<std::string> &/*params*/) {
    std::string session_id = "";
    for (auto [k,v] : request.get_query()) {
        if (k == "sid") {
            std::cout << "session " << v << "      (TODO)" << std::endl;
        }
    }
}




std::vector<std::string> get_local_addresses() {
    std::vector<std::string> ipv4_addresses;
    struct ifaddrs *ifaddr, *ifa;
    int family;
    if (getifaddrs(&ifaddr) == -1) {
        throw std::invalid_argument("Error: getifaddrs failed");
        return ipv4_addresses;
    }
    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
        if (ifa->ifa_addr == NULL)
            continue;
        family = ifa->ifa_addr->sa_family;
        if (family == AF_INET) {
            struct sockaddr_in *ipv4_addr = (struct sockaddr_in *)ifa->ifa_addr;
            char ip_str[INET_ADDRSTRLEN];
            inet_ntop(AF_INET, &(ipv4_addr->sin_addr), ip_str, INET_ADDRSTRLEN);
            if (std::string(ip_str) != std::string("127.0.0.1"))
                ipv4_addresses.push_back(ip_str);
        }
    }
    freeifaddrs(ifaddr);
    return ipv4_addresses;
}


int main(int argc, char **argv) {
    using namespace pigcd;
    using namespace std::chrono_literals;
    #if defined(ARCH_armv7l) || defined(ARCH_aarch64)
    std::cout << "Starting IP show..."<< std::endl;
    std::thread periodic_ip_update([](){
        std::cout << "Starting IP show... OK"<< std::endl;
        display_ssd1306_t ssddisp;
        int i = 0;
        while (finish_main_threads == 0) {
            for (int y = 9; y < 28; y+= 9) {
                ssddisp.print_string(0,y,"                ");
            }
            if (i % 2) {
                ssddisp.print_string(0,0,"Frezia IP: ");
            } else {
                ssddisp.print_string(0,0,"Frezia IP :");
            }
            int y = 9;
            for (auto s : get_local_addresses()) {
                ssddisp.print_string(0,y,s);
                y+= 9;
            }
            ssddisp.blit();
            std::this_thread::sleep_for(20s);
            i++;
        }
        std::cout << "Finished..."<< std::endl;
    });
    #else
        std::cout << "Not running on ARM" << std::endl;
    #endif

    if (config::from_args(argc, argv) < 0) return 0;
    
    std::ifstream cfg(config::config_file_name);
    if (cfg.is_open()) {
        config::load(cfg);
    } else {
        std::cerr << "Cannot open: '" << config::config_file_name  << "', creating the default one."<< std::endl;
        std::ofstream cfg(config::config_file_name);
        if (cfg.is_open()) config::save(cfg);
        else std::cerr << "Cannot create: '" << config::config_file_name  << "'" << std::endl;
    }
    config::from_args(argc, argv);  ///< make sure that the arguments from CLI are more important than the ones from file
    config::save(std::cout);
    motors::init();
    gpio::init_in(buttons::io_buttons, std::vector<int>(buttons::io_buttons.size(), 1));

    auto steps_thread = motors::run_thread();
    std::string line;
    signal(SIGINT, on_ctrl_c_signal);
    signal(SIGUSR1, on_usr1_signal);

    webapi::mini_http_c srv(webapi::server_host, webapi::server_port);

    // pigcd::webapi::add_api_doc_handler();
    srv.on_request("GET", ".*", session_handling_filter, "Session handling", "Every request can work with session. You should provide sid in query param.");
    srv.on_request("POST", ".*", session_handling_filter, "Session handling", "Every request can work with session. You should provide sid in query param.");
    srv.on_request("GET", "/api/gcode_status", request_handler_GET_api_gcode_status_all, "Execution status", "Returns the status for the g-code execution.");
    srv.on_request(
        "GET", "/api/togcd_status", [](auto &, auto &response, const auto &) { serve_file(togcd_status_filename, response); }, "G-code generation status",
        "Returns the status of g-code generation.");
    srv.on_request(
        "GET", "/api/future/([[:digit:]]+)",
        [&srv](pigcd::webapi::request_data_t &, pigcd::webapi::response_data_t &response, const std::vector<std::string> &params) {
            try {
                response.send(get_execution_task(params.at(1)).dump());
                // response.send("somesing");
            } catch (std::exception &e) {
                std::string error;
                error = error + "error parsing params: " + e.what() + ": ";
                for (auto p : params) {
                    error = error + "[" + p + "] ";
                }
                std::cerr << error << std::endl;
                throw std::invalid_argument(error);
            }
        },
        "Get the future result", "Returns the status of the task with given id. For example, the togcd endpoint generates future result.");
    srv.on_request("GET", "/api/gcode_status/([[:digit:]]+)", request_handler_GET_api_gcode_status, "Remaining execution status",
                   "Returns the execution status for every line starting with ([[:digit:]]+).");
    srv.on_request(
        "GET", "/apidoc",
        [&srv](pigcd::webapi::request_data_t &, pigcd::webapi::response_data_t &response, const std::vector<std::string> &) {
            response.send(srv.generate_apidoc());
        },
        "WebAPI Documentatino", "Generates the documentation page.");
    srv.on_request("POST", "/api/exec", request_handler_POST_exec, "Start gcode",
                   "Executes the gcode program. There can be only one executing, so if machine is running some gcode program, it will return error.");
    srv.on_request("POST", "/api/togcode", request_handler_POST_image_to_gcode, "Generate gcode",
                   "Generates gcode based on the parameters and the image sent as a base64.");

//
    srv.on_request("GET", "/api/getwifilist", request_handler_GET_wifi_list, "Get wifi list", "curl http://localhost:12112/api/getwifilist");
    srv.on_request("POST", "/api/setwifipass", request_handler_POST_set_wifi_password, "Setup wifi password",
                   "curl -d '{\"ssid\":\"demossid\",\"psk\":\"demopsk\"}' http://localhost:12112/api/setwifipass -H \"Content-Type: application/json\"");

    srv.on_request("GET", ".*", pigcd::webapi::handle_static, "Static contents", "Returns the static demo for the API");

    // everything is ready. Start server
    srv.server([]() { return !(pigcd::motors::is_thread_working == 0); });

    pigcd::motors::is_thread_working = false;
    int ret = steps_thread.get();

    motors::disable_spindle(0, unit_conversion::seconds_to_ticks(1));
    finish_main_threads = 1;
    return ret;
}
