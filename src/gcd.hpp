/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#ifndef ___GCD_TP_PIGCD__HPP______
#define ___GCD_TP_PIGCD__HPP______

#include <string>
#include <atomic>

#include "unit_conversion.hpp"
//#include <map>

namespace pigcd {
std::string exec_gcd_line(std::string line, std::atomic<bool> *break_current_execution_flag = nullptr);

}  // namespace pigcd

#endif
