/**
 * @file motors.hpp
 * @author Tadeusz Puźniakowski
 * @brief Module responsible for generating steps and making them smooth
 * @version 0.1
 * @date 2022-01-31
 *
 * @copyright Copyright (c) 2022
 *
 */
/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
#ifndef __MOTORS_HPP_TP___
#define __MOTORS_HPP_TP___

#include <algorithm>
#include <array>
#include <chrono>
#include <future>
#include <stdexcept>
#include <thread>
#include <vector>

#include "vec3di.hpp"

namespace pigcd {

inline void set_thread_realtime();

uint64_t get_precision_time();

namespace motors {

using machine_position_t = tp::discrete::vec3di_t;

extern std::vector<int> io_steppers_step;
extern std::vector<int> io_steppers_dir;
extern std::vector<int> io_steppers_en;
extern std::vector<int> io_spindles;
extern uint64_t step_duration;  ///< nanosecond delay between ticks

extern volatile std::atomic<int> is_thread_working;
extern std::future<int> run_thread();

machine_position_t get_global_machine_steps();
void set_global_machine_steps(const machine_position_t &steps);

void init();

// void enable_motors();
// void disable_motors();
void enable_spindle(int i, uint64_t delay);
void disable_spindle(int i, uint64_t delay);

enum raw_exec_steps_flag_e { NONE = 0, PROBE = 1, PROBE_X = 3, PROBE_Y = 5, PROBE_Z = 9, PROBE_XYZ = 15 };

int raw_exec_steps(const machine_position_t steps_,
                   raw_exec_steps_flag_e flags);  // = NONE);

void reset_probe_status();
std::pair<machine_position_t, int> get_probe_status();
void set_probe_status(const std::pair<machine_position_t, int> &status);

void exec_parking_pocedure(int x, int y, int z);

}  // namespace motors

}  // namespace pigcd

#endif
