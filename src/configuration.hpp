/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
#ifndef ___PIGCD_HPP____CONFIGURATION__
#define ___PIGCD_HPP____CONFIGURATION__

#include <sstream>

#include "motors.hpp"

namespace pigcd {
namespace config {
/**
 * @brief default configuration filename. This can be changed using CLI.
 *
 */
extern std::string config_file_name;

int from_args(int argc, char **argv);

int load(std::istream &cfg);
int save(std::ostream &f);

std::string generate_help_screen(std::string header, std::string footer, unsigned screen_width);

}  // namespace config
}  // namespace pigcd
#endif
