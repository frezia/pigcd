/**
 * @file unit_conversion.hpp
 * @author Tadeusz Puźniakowski
 * @brief Operations that allow for conversion between Euclidean space and
 * machine steps space
 * @version 0.1
 * @date 2022-01-31
 *
 * @copyright Copyright (c) 2022
 *
 */
/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
#ifndef ___UNIT_CONVERSION_HPP____
#define ___UNIT_CONVERSION_HPP____

#include <distance_t.hpp>
#include <functional>

#include "motors.hpp"

namespace pigcd {

namespace unit_conversion {
using position_t = raspigcd::generic_position_t<double, 3>;  // std::array<double, 3>;

extern position_t steps_per_millimeter;  // todo
const position_t scales_ = {1.0, 1.0, 1.0};

/**
 * @brief gives the time between steps in stepper motors
 *
 * @return double time in seconds between steps. Usuali it is some small
 * fraction.
 */
inline double dt() { return (double)motors::step_duration / (double)1000000000; };

inline uint64_t seconds_to_ticks(double t) { return t * 1000000000.0; };

/**
 * @brief function that converts coordinates into steps to perform
 *
 */
extern std::function<motors::machine_position_t(const position_t& distances_)> to_steps;

/**
 * @brief function that converts machine steps into coordinates in xyz
 *
 */
extern std::function<position_t(const motors::machine_position_t& steps_)> to_xyz;

/**
 * @brief converts coordinates for core xy machine layout
 *
 * @param distances_
 * @return motors::machine_position_t
 */
motors::machine_position_t cartesian_to_steps_corexy(const position_t& distances_);

/**
 * @brief converts coordinates to core xy machine layout
 *
 * @param steps_
 * @return position_t
 */
position_t steps_to_cartesian_corexy(const motors::machine_position_t& steps_);

/**
 * @brief converts distance to machine steps
 *
 * @param distances_
 * @return motors::machine_position_t
 */
motors::machine_position_t cartesian_to_steps(const position_t& distances_);

/**
 * @brief converts machine steps to distance
 *
 * @param steps_
 * @return position_t
 */
position_t steps_to_cartesian(const motors::machine_position_t& steps_);

}  // namespace unit_conversion
}  // namespace pigcd

#endif
