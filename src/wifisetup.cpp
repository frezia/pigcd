#include "wifisetup.hpp"

#include <stdio.h>
#include <vector>
#include <string>
#include <regex>
#include <iostream>
#include <streambuf>
#include <cstdio>
#include <set>
#include <fstream>
#include <map>
#include <thread>
#include <chrono>

namespace pigcd {

const std::string WPA_SUPPLICANT_CONF = "/etc/wpa_supplicant/wpa_supplicant.conf";

std::vector<std::string> get_wifi_list() {
    using namespace std::chrono_literals;
    std::set<std::string> ret;
    char line[4096];
    FILE *w_scan_f;
    w_scan_f = popen("wpa_cli scan", "r");
    if (w_scan_f == nullptr) return {};
    while (fgets(line, sizeof(line), w_scan_f)) { }
    pclose(w_scan_f);
    std::this_thread::sleep_for(1s);
    FILE *w_scan_result_f = popen("wpa_cli scan_results", "r");
    if (w_scan_result_f == nullptr) return {};
    while (fgets(line, sizeof(line), w_scan_result_f)) {
        std::regex reg_essid("[0-9a-z:]+\t[0-9]+\t.[0-9]+\t\\[.*\\]\t(.*)");
        std::smatch match;
        std::string l(line);
        if (std::regex_search(l, match, reg_essid)) {
            ret.insert(match[1]);
        }
    }
    pclose(w_scan_result_f);

    return std::vector<std::string>(ret.begin(), ret.end());
}


std::map<std::string, std::string> get_wpa_passwords() {
    std::map<std::string, std::string> ret;
    std::ifstream wpa_supplicant_conf (WPA_SUPPLICANT_CONF);
    if (!wpa_supplicant_conf.is_open()) return {};
    std::string line;

    std::string ssid;

    while (std::getline(wpa_supplicant_conf, line)) {
        std::regex reg_essid("ssid=\"(.*)\"");
        std::regex reg_psk("psk=\"(.*)\"");
        std::smatch match;
        std::string l(line);
        if (std::regex_search(l, match, reg_essid)) {
            ssid = match[1];
        } else if (std::regex_search(l, match, reg_psk)) {
            if (ssid != "") ret[ssid] = match[1];
        }
    }
    return ret;
}


void set_wpa_password(std::string ssid, std::string password) {
    std::vector<std::string> wpa_supplicant_lines;
    std::ifstream wpa_supplicant_conf (WPA_SUPPLICANT_CONF);
    if (wpa_supplicant_conf.is_open()) {
        std::string line;
        while (std::getline(wpa_supplicant_conf, line)) {
            wpa_supplicant_lines.push_back(line);
        }
    }

    std::string last_ssid = "";
    bool found_ssids = false;
    for (auto &line : wpa_supplicant_lines) {
        std::regex reg_essid("ssid=\"(.*)\"");
        std::regex reg_psk("psk=\"(.*)\"");
        std::smatch match;
        std::string l(line);
        if (std::regex_search(l, match, reg_essid)) {
            last_ssid = match[1];
        } else if (std::regex_search(l, match, reg_psk)) {
            if (last_ssid == ssid) {
                line = "    psk=\"" + password + "\"";
                found_ssids = true;
            }
        }
    }
    if (!found_ssids) {
        wpa_supplicant_lines.push_back("network={");
        wpa_supplicant_lines.push_back("    ssid=\"" + ssid + "\"");
        wpa_supplicant_lines.push_back("    psk=\"" + password + "\"");
        wpa_supplicant_lines.push_back("}");
    }

    std::ofstream wpa_supplicant_conf_new (WPA_SUPPLICANT_CONF);
    for (auto l : wpa_supplicant_lines) {
        wpa_supplicant_conf_new << l << std::endl;
    }
    wpa_supplicant_conf_new.close();

    FILE *wpa_cli_f = popen("wpa_cli reconfigure", "r");
    if (wpa_cli_f == nullptr) return;
    char line_reconf[4096]; size_t n;
    while((n = fread(line_reconf, 1, sizeof(line_reconf), wpa_cli_f)) > 0) {
       line_reconf[n] = 0;
       std::cout << line_reconf << std::endl;
    }
    pclose(wpa_cli_f);
    return;
}


}  // namespace pigcd

#ifdef _TEST__
int main() {
    using namespace pigcd;

    set_wpa_password("satoshi", "mysieniedamynikomu");

    auto cards_and_wifis = get_wifi_list();
    for (auto s : cards_and_wifis) {
        std::cout << "***" << s << "***" << std::endl;
    }

    auto configured_ssids = get_wpa_passwords();
    for (auto [s,k] : configured_ssids) {
        std::cout << "***" << s << ":" << k << "***" << std::endl;
    }

    return 0;
}
#endif


