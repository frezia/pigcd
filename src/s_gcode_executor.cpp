#include "s_gcode_executor.hpp"
/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <json.hpp>

#include "buttons.hpp"
#include "configuration.hpp"
#include "gcd.hpp"
#include "gpio.hpp"
#include "motors.hpp"
#include "unit_conversion.hpp"
#include "vec3di.hpp"

namespace pigcd {
namespace service {

void gcode_executor_t::push_back(const std::string s) {
    std::lock_guard<std::mutex> guard(_mutex);

    _status_exec.push_back(s);
}
void gcode_executor_t::clear() {
    std::lock_guard<std::mutex> guard(_mutex);

    _status_exec.clear();
}
std::size_t gcode_executor_t::exec_index() {
    std::lock_guard<std::mutex> guard(_mutex);
    return _status_exec.size();
}

int gcode_executor_t::gcode_executor(gcode_executor_t *pthis, const std::list<std::string> lines) {
    using namespace pigcd;
    using namespace std::chrono_literals;
    pthis->_lines = std::vector<std::string>(lines.begin(), lines.end());
    pthis->clear();
    for (auto line_p = lines.begin(); (line_p != lines.end()) && (pigcd::motors::is_thread_working); line_p++) {
        while ((pthis->_on_hold) && (!pthis->ignore_next_gcode) && (pigcd::motors::is_thread_working)) {
            std::this_thread::sleep_for(100ms);
        }
        std::stringstream ret_stream;
        std::string line = *line_p;
        if (line.size() > 0) {
            if (line[0] == ';') {
                ret_stream << "ok comment";
            } else if (!pthis->ignore_next_gcode) {
                try {
                    ret_stream << pigcd::exec_gcd_line(line, &(pthis->ignore_next_gcode));
                } catch (const std::exception &e) {
                    ret_stream << "ERROR EXECUTING '" << line << "': " << e.what();
                }
            } else if (pthis->ignore_next_gcode) {
                ret_stream << "ignorng command";
            }
        }
        pthis->push_back(ret_stream.str());
    }
    if (!pigcd::motors::is_thread_working) pthis->push_back("error: thread terminated");
    else {pthis->ignore_next_gcode = false;}
    return lines.size();
}

nlohmann::json gcode_executor_t::cycle_hold(std::list<std::string>) {
    _on_hold = true;
    return {{"status", "ok"}, {"note", "Cycle hold. Send single ! (exclamation mark) to resume."},{"hold",true}};
}
nlohmann::json gcode_executor_t::cycle_start(std::list<std::string>) {
    _on_hold = false;
    return {{"status", "ok"}, {"note", "Cycle start."},{"hold",false}};
}
nlohmann::json gcode_executor_t::cycle_cancel(std::list<std::string>) {
    _on_hold = false;
    ignore_next_gcode = true;
    return {{"status", "ok"}, {"note", "Cancelled execution!"},{"hold",false}};
}
nlohmann::json gcode_executor_t::current_status_compact(std::list<std::string>) {
    auto p_steps = pigcd::motors::get_global_machine_steps();
    auto p_mm = pigcd::unit_conversion::to_xyz(p_steps);
    auto buttons = pigcd::buttons::get_buttons_state();

    std::string buttons_txt;
    std::transform(buttons.begin(), buttons.end(), std::back_inserter(buttons_txt), [](auto e) { return (char)(e + '0'); });

    int idx = exec_index();
    bool hold = _on_hold;

    return {{"status", std::string("<") + (is_gcode_executing() ? "Executing" : "Idle") + ",MPos:" + std::to_string(p_mm[0]) + "," + std::to_string(p_mm[1]) +
                           "," + std::to_string(p_mm[2]) + ",WPos:" + std::to_string(p_mm[0]) + "," + std::to_string(p_mm[1]) + "," + std::to_string(p_mm[2]) +
                           ",MSteps:" + std::to_string(p_steps[0]) + "," + std::to_string(p_steps[1]) + "," + std::to_string(p_steps[2]) +
                           ",Buttons:" + buttons_txt + ",Index:" + std::to_string(idx) + ">"},
            {"position", {{"x", p_mm[0]}, {"y", p_mm[1]}, {"z", p_mm[2]}}},
            {"steps", {{"x", p_steps[0]}, {"y", p_steps[1]}, {"z", p_steps[2]}}},
            {"buttons", buttons},
            {"exec_index", idx},
            {"executing", is_gcode_executing()},
            {"hold", hold},
            {"id",(int)_program_id}};
}

bool gcode_executor_t::is_gcode_executing() { return ((_execution.valid()) && (_execution.wait_for(std::chrono::seconds(0)) != std::future_status::ready)); }

/**
 * @brief Get the status object that contains executed commands starting with
 * first_idx
 *
 * @param first_idx first index to get
 * @return nlohmann::json
 */
nlohmann::json gcode_executor_t::get_status(unsigned first_idx) {
    std::lock_guard<std::mutex> guard(_mutex);
    std::vector<nlohmann::json> ret;
    ret.reserve(_lines.size());
    for (auto i = first_idx; i < std::min(_status_exec.size() + 1, _lines.size()); i++) {
        nlohmann::json value = {{"command", _lines[i]}, {"i", i}};
        if (i < _status_exec.size()) value["log"] = _status_exec[i];
        ret.push_back(value);
    }
    bool hold = _on_hold;
    return {{"log", ret}, {"id", (int)_program_id}, {"exec_index",_status_exec.size()},{"executing", is_gcode_executing()}, {"hold", hold}};
}

nlohmann::json gcode_executor_t::exec(const std::list<std::string> &lines) {
    if (lines.size() == 1) {
        if (*lines.begin() == "!") {  ///< cycle hold - pause execution of gcode
            return cycle_hold(lines);
        } else if (*lines.begin() == "~") {  ///< cycle start
            return cycle_start(lines);
        } else if (*lines.begin() == "?") {  ///< current status
            return current_status_compact(lines);
        } else if (*lines.begin() == "&") {  ///< cancel running gcode
            return cycle_cancel(lines);
        } 
    }
    std::lock_guard<std::mutex> guard(_mutex);

    if (_execution.valid()) {
        if (!(_execution.wait_for(std::chrono::seconds(0)) == std::future_status::ready)) {
                bool hold = _on_hold;
            return {{"status", "error"}, {"error", "Only one G-CODE program can be run at once"}, {"executing", is_gcode_executing()},
            {"hold", hold}};
        }
    }
    _program_id = (_program_id + 1) & 0x07fffffff;
    _execution = std::async(std::launch::async, gcode_executor, this, lines);
    bool hold = _on_hold;

    return {{"status", "ok"},
            {"id", (int)_program_id},
            {"note", "Running in parallel. You can check the status by "},
            {"executing", true},
            {"exec_index", 0},
            {"hold", hold}};
}
}  // namespace service
}  // namespace pigcd
