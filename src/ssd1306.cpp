#include "ssd1306.hpp"
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include <ifaddrs.h>
#include <arpa/inet.h>

#include <string>
#include <vector>
#include <iostream>

namespace pigcd {

#include "font8x8_basic.h"
void display_ssd1306_t::write_buff(const std::vector < unsigned char > data) {
    if (ioctl(_handle, I2C_SLAVE, _addr) < 0) throw "error in ioctl(_handle, I2C_SLAVE, _addr) < 0";
    else write(_handle, data.data(), data.size());
}

void display_ssd1306_t::blit() {
    write_buff({0x00,0x21,0x00,0x7F,0x22,0x00,0x07}); // write buffer sequence
    for(auto &line:_display) write_buff(line);
}

void display_ssd1306_t::put_bit(int x, int y, int c) {
    if ((x < 0) || (x >= 128) || (y < 0) || (y >= 64)) return;
    int prev = _display[y>>3][x+1];
    prev = c*(prev | (1 << (y&7))) | (1-c)*(prev & (~(1 << (y&7))));
    _display[y>>3][x+1] = prev;
}

void display_ssd1306_t::print_bitmap_8x8(int x0, int y0, const unsigned char *p) {
    for (int y = 0; y < 8; y++) {
        int row = p[y];
        for (int x = 0; x < 9; x++) {
            int val = row & 1;
            put_bit(x+x0,y+y0,val);
            row = row >> 1;
        }
    }
}

/**
 * @brief Print single digit on a buffer
 * 
 * @param x position x
 * @param y position y
 * @param digit digit. It only supports '0'-'9', '.', ' '
 */
void display_ssd1306_t::print_character(int x, int y, char digit) {
    const unsigned char *p;
    p = (unsigned char *)(font8x8_basic[digit & 0x07f]);
    print_bitmap_8x8(x,y,p);
}

void display_ssd1306_t::print_string(int x, int y, const std::string s) {
    const char *digits = s.c_str();
    while (*digits) {
        print_character(x,y,*digits);
        digits++;
        x+=8;
    }
}


display_ssd1306_t::display_ssd1306_t(const int address_ , const std::string &devfile_ ) {
    _addr = address_;
    _handle = open(devfile_.c_str(), O_RDWR);
    if (_handle < 0) throw std::invalid_argument("error opening i2c device");
    write_buff({0x00,0xAE,0xA8,0x3F,0xD3,0x00,0x40,0xA1,0xC8,0xDA,0x12,0x81,0x7F,0xA4,0xA6,0xD5,0x80,0x8D,0x14,0xD9,0x22,0xD8,0x30,0x20,0x00,0xAF}); ///< initialize device
    _display = std::vector<std::vector<unsigned char>>(8,std::vector<unsigned char>(129));
    for(auto &line: _display) line[0] = 0x40; // first byte in each line must be set to 0x040
}

display_ssd1306_t::~display_ssd1306_t() {
    close(_handle);
}


}